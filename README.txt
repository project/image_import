
IMPORTANT NOTE: Conversion of this module to Drupal 4.7 is NOT YET COMPLETE.
This version is an interim commit of work in progress, and not yet fully
functional.


image_import.module	Allows mass import of images and, optionally, captions
			into Drupal installations using the improved image
			module from James Walker (drupal.org name "walkah").

Author:			Scott Courtney (drupal.org name "syscrusher")
			email: scott (at) 4th (dot) com

License:		GNU General Public License (GPL) http://gnu.org/

Status:			BETA release for initial testing

Prerequisites:		upload.module, image.module ("walkah" version
			required), and Drupal 4.6, all configured and
			working properly. Previous versions of Drupal
			are not supported.

Features:

* Number of images that can be imported is limited only by the system's
  speed and settings (such as PHP's maximum script execution time).

* Fully supports and exploits free-tag vocabularies using Morbus Iff's
  patch, if available. Also works correctly without that patch.

* Unlike previous image import code, this module can do a "fast import"
  while also individually captioning images. For each image being
  imported, the module looks for a file whose name matches but whose
  extension is one of several typical text file types. If found, that
  file's contents will be used to automatically create a teaser and
  body (description) for the image.

* Optionally deletes the uploaded files (including caption files), to
  prevent the next import run from accidentally importing them again
  as duplicate nodes.

* System administrators can force the deletion of uploaded files.

* To support systems where users have individual upload directories,
  the module can be configured to insert the Drupal username into
  the import directory path. It can also optionally *not* do this
  for the special case of the system user (uid=1). There is also
  a way to have the module remove spaces from the usernames.

* Users with appropriate permissions, as defined by the site owner,
  can override the import path for their images.

* Extensive descriptions and help for all administrative and
  user-accessible form fields.

* A separate detailed help page (Drupal path node/add/image_import/help)
  provides a step-by-step guide to using the module, with the instructions
  customized based on the individual user name and security privileges as
  well as various site-wide settings from image_import and the modules
  it uses internally.

* Optional throttle mode ensures that each image node is created with
  a different timestamp, since some galleries use this for sorting
  them.

* Optional time limit on import helps prevent script timeout errors in
  situations where the Drupal administrator does not have control over
  PHP settings. The user can restart the import where it left off, to
  continue importing images. (This feature only works if the images are
  being deleted during import.)

* Optional debug mode provides verbose diagnostics to help isolate
  problems with file permissions and similar issues.
